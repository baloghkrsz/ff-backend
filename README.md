# README #

Family Finances Node.js server application

### How do I get set up? ###

* Check out from repository
* Install dependencies - run "npm install"
* Configure the app in the .env file
* Start the application - npm start
